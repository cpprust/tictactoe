mod assets;
mod gui;
mod tictactoe;

use iced::Sandbox;

use crate::gui::TictactoeApp;

fn main() -> Result<(), iced::Error> {
    TictactoeApp::run(gui::settings())
}
