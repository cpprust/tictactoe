use iced::alignment::{Horizontal, Vertical};
use iced::widget::{Button, Column, Row, Text};
use iced::{Element, Font, Length, Sandbox};

use crate::tictactoe::Tictactoe;

pub struct TictactoeApp {
    tictactoe: Tictactoe,
}

impl Sandbox for TictactoeApp {
    type Message = Message;

    fn new() -> Self {
        Self {
            tictactoe: Tictactoe::new(),
        }
    }

    fn title(&self) -> String {
        String::from("Tictactoe")
    }

    fn update(&mut self, message: Self::Message) {
        match message {
            Message::ButtonPressed(x, y) => self.tictactoe.act_on(x, y),
        }
    }

    fn view(&self) -> Element<'_, Self::Message> {
        let mut column = Column::new();
        for x in 0..3 {
            let mut row = Row::new();
            for y in 0..3 {
                let cell_value = match &self.tictactoe.cells[x][y] {
                    Some(cell_value) => cell_value,
                    None => "",
                };
                let button_text = Text::new(cell_value)
                    .width(Length::Fill)
                    .height(Length::Fill)
                    .horizontal_alignment(Horizontal::Center)
                    .vertical_alignment(Vertical::Center)
                    .size(40.0)
                    .font(Font {
                        family: iced::font::Family::Monospace,
                        weight: iced::font::Weight::Bold,
                        ..Default::default()
                    });
                let button = Button::new(button_text)
                    .on_press(Message::ButtonPressed(x, y))
                    .width(Length::Fill)
                    .height(Length::Fill)
                    .style(iced::theme::Button::Secondary);
                row = row.push(button).height(iced::Length::Fill);
            }
            column = column.push(row).width(iced::Length::Fill);
        }
        Element::from(column)
    }
}

pub fn settings() -> iced::Settings<()> {
    let tictactoe_image = image::load_from_memory(crate::assets::TICTACTOE_IMAGE).unwrap();
    let tictactoe_iamge_width = tictactoe_image.width();
    let tictactoe_image_height = tictactoe_image.height();
    iced::Settings {
        window: iced::window::Settings {
            icon: Some(
                iced::window::icon::from_rgba(
                    tictactoe_image.into_rgba8().into_vec(),
                    tictactoe_iamge_width,
                    tictactoe_image_height,
                )
                .unwrap(),
            ),
            ..Default::default()
        },
        ..Default::default()
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Message {
    ButtonPressed(usize, usize),
}
