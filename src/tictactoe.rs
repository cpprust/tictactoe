pub struct Tictactoe {
    pub cells: [[Option<String>; 3]; 3],
    current_chess: char,
    game_over: bool,
}

impl Tictactoe {
    const PLAYER_1_CHESS: char = 'X';
    const PLAYER_2_CHESS: char = 'O';

    pub fn new() -> Self {
        Self {
            cells: [[None, None, None], [None, None, None], [None, None, None]],
            current_chess: Self::PLAYER_1_CHESS,
            game_over: false,
        }
    }

    pub fn act_on(&mut self, x: usize, y: usize) {
        if self.game_over {
            self.reset();
            return;
        }

        self.place_chess(x, y);

        let winning_indexes = self.winning_indexes();
        winning_indexes.iter().for_each(|winning_indexes| {
            self.mark_cells(winning_indexes);
            self.game_over = true;
        });

        if self.full() {
            self.game_over = true;
        }
    }

    fn reset(&mut self) {
        self.current_chess = Self::PLAYER_1_CHESS;
        self.cells.iter_mut().for_each(|row| {
            row.iter_mut().for_each(|cell| {
                *cell = None;
            })
        });
        self.game_over = false;
    }

    fn change_side(&mut self) {
        if self.current_chess == Self::PLAYER_1_CHESS {
            self.current_chess = Self::PLAYER_2_CHESS;
        } else {
            self.current_chess = Self::PLAYER_1_CHESS;
        }
    }

    fn place_chess(&mut self, x: usize, y: usize) {
        if self.cells[x][y] != None {
            return;
        }
        self.cells[x][y] = Some(self.current_chess.to_string());
        self.change_side();
    }

    fn full(&self) -> bool {
        for x in 0..3 {
            for y in 0..3 {
                if self.cells[x][y] == None {
                    return false;
                }
            }
        }
        true
    }

    fn winning_indexes(&self) -> Option<Vec<(usize, usize)>> {
        for x in 0..3 {
            if self.cells[x][0].is_some()
                && self.cells[x][0] == self.cells[x][1]
                && self.cells[x][1] == self.cells[x][2]
            {
                return Some(vec![(x, 0), (x, 1), (x, 2)]);
            }
        }
        for y in 0..3 {
            if self.cells[0][y].is_some()
                && self.cells[0][y] == self.cells[1][y]
                && self.cells[1][y] == self.cells[2][y]
            {
                return Some(vec![(0, y), (1, y), (2, y)]);
            }
        }
        if self.cells[0][0].is_some()
            && self.cells[0][0] == self.cells[1][1]
            && self.cells[1][1] == self.cells[2][2]
        {
            return Some(vec![(0, 0), (1, 1), (2, 2)]);
        }
        if self.cells[2][0].is_some()
            && self.cells[2][0] == self.cells[1][1]
            && self.cells[1][1] == self.cells[0][2]
        {
            return Some(vec![(2, 0), (1, 1), (0, 2)]);
        }

        None
    }

    fn mark_cells(&mut self, indexes: &Vec<(usize, usize)>) {
        indexes.iter().for_each(|(x, y)| {
            self.cells[*x][*y]
                .iter_mut()
                .for_each(|cell_value| *cell_value = format!(">{}<", cell_value));
        });
    }
}
